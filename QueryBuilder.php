<?php

class QueryBuilder
{
    protected $collections = [];

    protected $where = [];

    public function where($column, $value)
    {
        echo sprintf("where %s = %s <br>", $column, $value);
        $this->where[] = compact('column', 'value');
        return $this;
    }

    public function getCollection()
    {
        if ($this->collections) {
            return $this->collections;
        }

        $this->collections =  [
            new User(['email' => 'alice@gmail.com', 'name' => 'Alice', 'age' => 24, 'gender' => 'female']),
            new User(['email' => 'bob@gmail.com', 'name' => 'Bob', 'age' => 24, 'gender' => 'male']),
            new User(['email' => 'Clever@gmail.com', 'name' => 'clever', 'age' => 24, 'gender' => 'female']),
        ];

        return $this->collections;
    }

    public function get()
    {
        return array_filter($this->getCollection(), function ($user) {
            foreach ($this->where as $condition) {
                $column = $condition['column'];
                if ($user->$column != $condition['value']) {
                    return false;
                }
                return true;
            }
        });
    }

    public function first()
    {
        $results = $this->get();
        return isset($results[0]) ? $results[0] : null;
    }
}