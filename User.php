<?php

class User extends Model
{
    public static function findUserByEmail($email)
    {
        echo sprintf("findUserByEmail: %s <br>", $email);
        return static::where('email', $email)->first();
    }

    public function __invoke()
    {
        echo 'Name: ' . $this->name . '<br>';
        echo 'Email: ' . $this->email;
    }
    
    public function __toString()
    {
        return '';
    }
}