<?php

class Model implements ArrayAccess
{
    protected $attributes = [];

    public function __construct($attributes = [])
    {
        $this->attributes = $attributes;
    }

    public static function __callStatic($method, $arguments)
    {
        $instance = new static();
        return $instance->$method(...$arguments);
//        return call_user_func_array([$instance, $method], $arguments);
    }

    public function __call($method, $arguments)
    {
        return $this->newQuery()->$method(...$arguments);
//        return call_user_func_array([$this->newQuery(), $method], $arguments);
    }

    public function newQuery()
    {
        return new QueryBuilder();
    }

    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->attributes);
    }

    public function offsetGet($offset)
    {
        if ($this->offsetExists($offset)) {
            return $this->attributes[$offset];
        }
    }

    public function offsetSet($offset, $value)
    {
        $this->attributes[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->attributes[$offset]);
    }

    public function __isset($name)
    {
        return isset($this->attributes[$name]);
    }

    public function __get($name)
    {
        return $this->offsetGet($name) ?: null;
    }

    public function __set($name, $value)
    {
        return $this->offsetSet($name, $value);
    }
}